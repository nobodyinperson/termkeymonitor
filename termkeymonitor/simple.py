# system modules
import sys
import asyncio
import os
import textwrap

# external modules
import evdev

# internal modules
import termkeymonitor
from termkeymonitor import (
    KEYMAPS,
    find_input_devices,
    key_pretty,
    make_argparser,
    stop_on_keyboardinterrupt,
)


def print_centered(s):
    s = textwrap.shorten(
        s, width=os.get_terminal_size().columns, placeholder="..."
    )
    s = s.center(os.get_terminal_size().columns - 1)
    sys.stdout.write(f"\r{s}")


@stop_on_keyboardinterrupt
def cli():
    parser = make_argparser()
    args = parser.parse_args()

    if not args.devices:
        print_centered("🔎 Searching for input devices...")
        args.devices = find_input_devices()
    print_centered(
        f"✅ Using input devices:{chr(10)}"
        f"{''.join([d.path+' ('+d.name+')' for d in args.devices])}",
    )

    active_keys = set()

    def show_active_keys():
        print_centered(
            " ".join(
                key_pretty(
                    code=code,
                    keymap=args.keymap,
                    show_code=args.show_code,
                )
                for code in active_keys
            )
        )

    async def handle_events(device):
        async for event in device.async_read_loop():
            if event.type == evdev.ecodes.EV_KEY:
                if event.value > 0:
                    active_keys.add(event.code)
                elif event.code in active_keys:
                    active_keys.remove(event.code)
                for key in ("REL_WHEEL_UP", "REL_WHEEL_DOWN"):
                    if key in active_keys:
                        active_keys.remove(key)
                show_active_keys()
            if event.type == evdev.ecodes.EV_REL:
                if event.code == evdev.ecodes.REL_WHEEL:
                    if event.value > 0:
                        active_keys.add("REL_WHEEL_UP")
                        if "REL_WHEEL_DOWN" in active_keys:
                            active_keys.remove("REL_WHEEL_DOWN")
                    else:
                        active_keys.add("REL_WHEEL_DOWN")
                        if "REL_WHEEL_UP" in active_keys:
                            active_keys.remove("REL_WHEEL_UP")
                    show_active_keys()

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    for device in args.devices:
        task = asyncio.Task(handle_events(device), loop=loop)
        asyncio.ensure_future(task)

    loop.run_forever()


if __name__ == "__main__":
    cli()
