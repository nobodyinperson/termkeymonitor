# ⌨️  Terminal Key Monitor 📟

Show pressed keys in the terminal. Useful e.g. when recording [asciinema screencasts](https://asciinema.org).

`termkeymonitor` connects directly to a local input device using `evdev`.

# 📥 Installation

```bash
# from PyPI
pip install termkeymonitor
# directly from GitLab
pip install git+https://gitlab.com/nobodyinperson/termkeymonitor.git
```

# ❓ Usage

```bash
# use any of the below commands to invoke the rich-version
termkeymonitor
python -m termkeymonitor
python3 -m termkeymonitor
python3 -m termkeymonitor.rich
# use this for the 'simple' version that uses less asciicast disk space
python -m termkeymonitor.simple
python3 -m termkeymonitor.simple
```

## 🎥 Screencast

[![asciicast](https://asciinema.org/a/544131.svg)](https://asciinema.org/a/544131)

## ⚠️ Caveats

- beware that the default `rich` variant causes each keypress to result in *multiple kBs* in an asciicast. Consider using `python -m termkeymonitor.simple` instead, which brings that number down to around the width of your terminal (≈100 bytes/keystroke).
- no Windows and MacOS support (Apparently `evdev` only works on Linux... 😔, might need to use some other backend like [`pynput`](https://pypi.org/project/pynput/) in the future)
- needs a local keyboard connected so doesn't work as expected on remote machines (e.g. via SSH)
- Make sure you have permissions to access the `/dev/input/event*` files (this normally means you need to add yourself to the `input` group)
